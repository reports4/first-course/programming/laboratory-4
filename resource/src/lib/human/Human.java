package lib.human;

import lib.enums.Feel;
import lib.exception.NullOutfitIntoHumanException;

/**
 * %Описнаие участника мероприятия%
 */
public abstract class Human implements DoHuman{

    private Outfit outfit;
    private Feel feel;
    private final String name;

    public Human(String name, Outfit outfit, Feel feel) {
        this(name);
        setDress(outfit);
        setFeel(feel);
    }

    public Human(String name, Outfit outfit) {
        this(name);
        setDress(outfit);
    }

    public Human(String name) {
        this.name = name;
        setDress(new Outfit("Default"));
        setFeel(Feel.NONE);
    }

    @Override
    public String toString() { return "Гость " + name; }

    @Override
    public int hashCode() {
        int sum = toString().hashCode() + outfit.hashCode();
        if(feel == Feel.CONSTRAINT) sum += 5*101;
        else if(feel == Feel.NONE) sum += 2*101;
        else if(feel == Feel.OK) sum += 3*101;

        return sum;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || this.getClass() != obj.getClass()) return false;
        Human h = (Human) obj;
        return this.feel == h.getFeel() && this.name.equals(h.getName()) && this.outfit == h.getOutfit();
    }

    /**
     * %Установаить наряд на это мероприятие%
     */
    public void setDress(Outfit outfit){
        if(outfit == null) throw new NullOutfitIntoHumanException();
        this.outfit = outfit;
    }

    /**
     * %Установить, как себя чувствовать%
     */
    public void setFeel(Feel feel){ this.feel = feel; }

    /**
     * %Оченить наряд другого участника%
     */
    public abstract void rateOutfit(Human other);

    /**
     * %Как себя чувствует%
     */
    public Feel getFeel() { return feel; }

    /**
     * %Одетый наряд%
     */
    public Outfit getOutfit() { return outfit; }

    /**
     * %Имя гостя%
     */
    public String getName(){ return name; }
}
