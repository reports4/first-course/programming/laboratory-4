package lib.human;

import lib.enums.Phrases;

public interface DoHuman {
    /**
     * %Поприветсвовать другого участника%
     */
    void great(Human other);
    /**
     * %Раскланиваться с другим участником%
     */
    void bowOut(Human other);
    /**
     * %Повторить особенную фразу другому участнику%
     */
    default void repeat(Human other, Phrases phrase){
        System.out.println(">" + this + " %обращается к %" + other.toString() + " %и говорить:% " + phrase.getPhrases());
    }
}
