package lib.human;

/**
 * Описание наряда на участнике
 */
public class Outfit {

    private final char quality;
    private final char evaluation;
    private final String name;

    public Outfit(String name) {
        quality = 'a';
        evaluation = '1';
        this.name = name;
    }
    public Outfit(String name, int quality, int evaluation) {
        this.name = name;
        this.quality = (char)Math.abs(quality);
        this.evaluation = (char)Math.abs(evaluation);
    }
    /**
     * %Получить качество наряда%
     */
    public int getQuality() { return quality; }
    /**
     * %Получить оценку наряду%
     */
    public int getEvaluation() { return evaluation; }
    /**
     * %Получить имя наряда%
     */
    public String getName() { return name; }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || this.getClass() != obj.getClass()) return false;
        Outfit h = (Outfit) obj;
        return this.quality == h.getQuality() && this.name.equals(h.getName()) && this.evaluation == h.getEvaluation();
    }
    @Override
    public int hashCode() { return (int)evaluation * 31 + (int) quality * 51; }
    @Override
    public String toString() { return "Одетый наряд" + name; }
}
