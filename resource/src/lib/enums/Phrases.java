package lib.enums;

public enum Phrases {
    FEELGOOD("%Как хорошо, что нет дождя%"),
    FIND("%Как хорошо, что сумка нашлась.%"),
    OTHER("%Как хорошо.%");
    
    private String phrases;
    Phrases(String phrases){ this.phrases = phrases; }
    public String getPhrases(){ return phrases; }
}
