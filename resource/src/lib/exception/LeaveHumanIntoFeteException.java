package lib.exception;

public class LeaveHumanIntoFeteException extends RuntimeException {

    private String message;

    public LeaveHumanIntoFeteException() { message = "%Выгонять некого с таким профилем. Даже клона нет.%"; }

    public LeaveHumanIntoFeteException(String message) { this.message = message; }

    @Override
    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
