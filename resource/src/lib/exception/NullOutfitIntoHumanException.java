package lib.exception;

public class NullOutfitIntoHumanException extends NullPointerException {

    private String message;

    public NullOutfitIntoHumanException() { message = "%Вы че? Это вам не пляж нудистов.%"; }

    public NullOutfitIntoHumanException(String message) { this.message = message; }

    @Override
    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
