package lib.fete;

import lib.exception.CountOfPeopleException;
import lib.human.Human;
import java.util.ArrayList;

public interface DoFete {
    /**
     * %Добавить участника на мероприятие%
     */
    void addParticipant(Human h);
    /**
     * %Уйти с мероприятия%
     */
    void leaveParticipant(Human h);
    /**
     * %Начать мероприятие%
     */
    void startFete() throws CountOfPeopleException;
    /**
     * %Закончить мероприятие%
     */
    void stopFate();
    /**
     * %Добавить множество гостей на мероприятие%
     */
    default void addParticipants(Human... hs){
        for (Human people: hs)
            addParticipant(people);
    }
    /**
     * %Убрать множество гостей с мероприятие%
     */
    default void leaveParticipants(ArrayList<Human> guest){
        for(int i = guest.size()-1; i > -1 ; i--)
            this.leaveParticipant(guest.get(i));
    }
}
