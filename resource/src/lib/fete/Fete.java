package lib.fete;

import lib.enums.Phrases;
import lib.enums.Situation;
import lib.Utils;
import lib.human.Human;
import java.util.*;

/**
 * %Описание Мероприятие%
 */
public abstract class Fete implements DoFete {

    private ArrayList<DoLater> doLaters = new ArrayList<>();
    private final Controller controller;

    protected ArrayList<Human> guests = new ArrayList<>(Utils.Const.SIZE);
    private Situation situation;
    private boolean startFete = false;
    
    public Fete(Situation situation, Human... guests) {
        this(situation);
        addParticipants(guests);
    }

    public Fete(Situation situation) {
        this.situation = situation;
        controller = new Controller();
    }

    /**
     * %Получить окружающую обстановку%
     */
    public Situation getSituation() { return situation; }

    public void setSituation(Situation s) { 
        situation = s;
        System.out.println(">>Окружающая обстановка изменилась на " + s);
        if (startFete && s != Situation.RAIN) {
            List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
            Human h1 = getRandomPeople.get(0);
            Human h2 = getRandomPeople.get(1);
            h1.repeat(h2, Phrases.FEELGOOD);
        }
    }

    public void tick(long time) {
        if ((time / Utils.Const.SECOND) > Utils.Const.WorkTimeS) {
            this.stopFate();
            return;
        }

        for (DoLater dl : doLaters)
            dl.complete(time / 1000000);

        List<Human> getRandomPeople = Utils.pickNRandomElements(guests);
        Human h1 = getRandomPeople.get(0);
        Human h2 = getRandomPeople.get(1);

        int randDO = new Random().nextInt(4);

        if (randDO == 0) { h1.great(h2); } 
        else if (randDO == 1) { h1.bowOut(h2); } 
        else if (randDO == 2) { h1.rateOutfit(h2); } 
        else if (randDO == 3) {
            Phrases phr = Utils.getRandomElementEnum(Phrases.class);
            do {
                if (getSituation() == Situation.RAIN && phr == Phrases.FEELGOOD)
                    phr = Utils.getRandomElementEnum(Phrases.class);
                else break;
            } while (true);
            h1.repeat(h2, phr);
        }
    }

    public void addDoLater(DoLater dolate) {
        if (!doLaters.contains(dolate) && dolate != null)
            doLaters.add(dolate);
    }

    /**
     * %Началось ли мероприятие%
     */
    public boolean isStartFete() { return startFete; }

    /**
     * %Установить началось ли мероприятие%
     */
    public void setStartFete(boolean startFete) { this.startFete = startFete; }

    public int sizeGuest() { return guests.size(); }

    public Controller getController(){ return controller; }

    public class Controller {

        private boolean isRunning;

        public Controller(){ isRunning = false; }

        public void start(){
            if(isRunning) return;
            startFete();
            run();
        }

        public void stop(){
            if(!isRunning) return;
            isRunning = false;
        }

        private void run(){
            isRunning = true;

            final double frameTime = 1.0 / Utils.Const.FRAMECAP;
            long lastTime = Utils.Time.getTime();
            final long startGame = lastTime;
            double unprocessedTime = 0;

            while(isRunning) {
                boolean render = false;
                long startTime = Utils.Time.getTime();
                long passedTime = startTime - lastTime;
                lastTime = startTime;
                unprocessedTime += passedTime / (double) Utils.Const.SECOND;
                while((unprocessedTime - frameTime) > 1e-8) {
                    render = true;
                    unprocessedTime -= frameTime;
                    if(!isStartFete()) { stop(); return; }
                }
                
                if(render) { tick(lastTime - startGame); }
                else {
                    try { Thread.sleep(1); } 
                    catch (InterruptedException e) { e.printStackTrace(); }
                }
            }
        }
    }
}
