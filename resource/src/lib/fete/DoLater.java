package lib.fete;

@FunctionalInterface
public interface DoLater {
    void complete(long reactionTimeMillis);
}
