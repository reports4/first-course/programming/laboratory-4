package lib;

import java.util.*;

public class Utils {

    public static <E extends Enum<E>> E getRandomElementEnum(Class<E> en) {
        Enum<E>[] all = en.getEnumConstants();
        int random = (int) (Math.random() * all.length);
        return (E) all[random];
    }

    public static<E> List<E> pickNRandomElements(final List<E> list) {
        ArrayList<E> returnList = new ArrayList<>(list);
        for (int i = 0; i < 5; i++)  Collections.shuffle(returnList);
        return returnList.subList(returnList.size() - 2, returnList.size());
    }

    public interface Const{
        long SECOND = (long)1e9;
        double FRAMECAP = 4.0f; //%кол-во действий в секунду%
        int SIZE = 256; // %Максимальное кол-во гостей%
        int WorkTimeS = 5; // %Время работы мероприятия%
    }

    public static class Time {
        public static long getTime() { return System.nanoTime(); }
        public static boolean isWork(long reactionTime){ return reactionTime < (1000 / Const.FRAMECAP) && reactionTime >= 0; }
    }
}
