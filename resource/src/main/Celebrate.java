package main;

import lib.enums.Situation;
import lib.exception.AddHumanIntoFeteException;
import lib.exception.CountOfPeopleException;
import lib.exception.LeaveHumanIntoFeteException;
import lib.fete.Fete;
import lib.human.Human;

public class Celebrate extends Fete {

    public Celebrate(Situation situation, Human ... guests) { super(situation, guests); }
    public Celebrate(Situation situation) { super(situation); }

    @Override
    public void addParticipant(Human h) {
        System.out.println(">" + h + " %приходит на мероприятие%");
        if(this.guests.contains(h)) throw new AddHumanIntoFeteException();
        this.guests.add(h);
    }
    @Override
    public void leaveParticipant(Human h) {
        System.out.println(">" + h + " %покидает мероприятие%");
        if(!this.guests.contains(h)) throw new LeaveHumanIntoFeteException();
        this.guests.remove(h);
    }
    @Override
    public void startFete() throws CountOfPeopleException{
        System.out.println("%>>Начинаем мероприятие"%);
        if(this.sizeGuest() == 0) throw new CountOfPeopleException("%Никого нет. Зачем вообще это надо?%");
        else if(this.sizeGuest() < 2) throw new CountOfPeopleException();
        this.setStartFete(true);
    }
    @Override
    public void stopFate() {
        System.out.println("%>>Заканчиваем мероприятие%");
        this.setStartFete(false);
        this.leaveParticipants(this.guests);
    }
}
