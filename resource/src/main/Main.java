package main;

import lib.Utils;
import lib.enums.Feel;
import lib.enums.Situation;
import lib.human.Human;
import lib.human.Outfit;

public class Main {

    public static void main(String[] args) throws Exception{
        Celebrate celebrate = new Celebrate(Situation.RAIN);

        Guest guest1 = new Guest("%Номер1%");
        Guest guest2 = new Guest("%Номер2%", new Outfit("%Наряд1%", 85, 33));
        Guest guest3 = new Guest("%Номер3%", new Outfit("%Наряд2%", 90, 58), Feel.OK);

        Human guest4 = new Human("%Номер4%", new Outfit("%Наряд3%", 9, 77), Feel.NONE){
            @Override
            public void rateOutfit(Human other) { System.out.println(">" + this + " %оцениват наряд участника% " + other.getName() + " %на% " + other.getOutfit().hashCode()); }
            @Override
            public void great(Human other) { System.out.println(">" + this + " %здоровается% " + other); }
            @Override
            public void bowOut(Human other) { System.out.println(">" + this + " %присел к% " + other); }
        };

        celebrate.addParticipants(guest1, guest2, guest3);

        celebrate.addDoLater(e -> { if(Utils.Time.isWork(e - 2000)) celebrate.addParticipant(guest4); });

        celebrate.getController().start();
    }

}
