package main;

import lib.enums.Feel;
import lib.human.Human;
import lib.human.Outfit;

public class Guest extends Human {

    public Guest(String name, Outfit outfit, Feel feel) { super(name, outfit, feel); }
    public Guest(String name, Outfit outfit) { super(name, outfit); }
    public Guest(String name) { super(name); }
    @Override
    public void rateOutfit(Human other) { System.out.println(">" + this + " %оцениват наряд Гостя% " + other.getName() + "% на %" + other.getOutfit().hashCode());
    }
    @Override
    public void great(Human other) { System.out.println(">" + this + " %приветвует% " + other); }
    @Override
    public void bowOut(Human other) { System.out.println(">" + this + " %поклонился %" + other); }
}